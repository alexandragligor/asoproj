import time
from subprocess import Popen, PIPE
import os
import shutil

version = 0

def _update():
    global version
    proc = Popen(["git", "push"], stdout=PIPE)
    (output, err) = proc.communicate()
    exit_code = proc.wait()

    if exit_code != 0:
        print(err)
    else:
        lines = output.decode('utf-8')
        if output.decode('utf-8').startsWith("Everything up-to-date"):
            print('No update')
        else:
            print('Building project...')
            os.chdir('ASO')
            proc = Popen(["mvn", "compile", "package", "-DskipTests"], stdout=PIPE)
            exit_code = proc.wait()
            os.chdir('..')
            if exit_code != 0:
                print('Build failed')
            else:
                if not os.path.exists("out/build_%d" %(version)):
                    os.mkdir("out/build_%d" %(version))
                shutil.copy2('ASO/target/FirstProjectASO-1.0-SNAPSHOT.jar', "out/build_%d/FirstProjectASO.jar" %(version))
                print('Artifact copied successfully')
                version += 1
                


def __main__():
    while True:
        _update()
        time.sleep(10)

if __name__ == '__main__':
    __main__()
